##          Basic libaries
import os
import sys
##          Framework
from tensorflow.keras.utils import plot_model
##          Files
from pathlib import Path
root_path=Path(os.getcwd())
sys.path.append('/home/doannn/Documents/Self_Learning/Self_Learning/DL_AI/Computer_Vision/FrameWorks/Tensorflow/Tensorflow_2.0/TF_2.0_Tutorials/Eat_TF2/Chapter1.ModelingProcedureOfTensorflow/Chapter1-2.ModelingProcedureForImages')
# print(root_path)
import configs_param
from models.lenet_5 import lenet

def plot_graph(model, img_name):
    plot_model(model, to_file=img_name, show_shapes=True, show_layer_names=True)


if __name__ == '__main__':
    model = lenet()
    img_name = './lenet5.png'
    plot_graph(model, img_name)