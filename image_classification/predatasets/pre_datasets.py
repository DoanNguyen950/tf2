##          basic libaries
import os
import pathlib
##          Framework
import tensorflow as tf
from tensorflow.keras import datasets, layers, models
##          files
import sys
import os
from pathlib import Path, PosixPath
current_path = Path(os.getcwd())
sys.path.append(str(current_path))
import configs_param


def create_datasets():
    """
    Notes:
        - class_mode='sparse'<-> 'sparse_categorical_crossentropy'
        - class_mode='categorical' <-> 'categorical_crossentropy'
        - data_dir:  = pathlib.Path(data_dir)
    """
    image_generator = tf.keras.preprocessing.image.ImageDataGenerator(rescale=1./255)
    ###         convert path (str) to path (pathlib.PosixPath)
    # print("Before convert: {}".format(type(configs_param.TRAIN_PATH)))
    train_dir = pathlib.Path(configs_param.TRAIN_PATH)
    valid_dir = pathlib.Path(configs_param.VALID_PATH)
    test_dir = pathlib.Path(configs_param.TEST_PATH)
    # print("After convert: {}".format(type(train_dir)))

    train_ds = tf.keras.preprocessing.image_dataset_from_directory(
        train_dir,  
        validation_split=0.2,   
        subset="training",  
        seed=123,
        image_size=(configs_param.INPUT_SIZE[0], configs_param.INPUT_SIZE[1]),
        batch_size=configs_param.BATCH_SIZE,
        # label_mode='categorical'
    )
    valid_ds = tf.keras.preprocessing.image_dataset_from_directory(
        valid_dir,
        validation_split=0.2,
        subset="validation",
        seed=123,
        image_size=(configs_param.INPUT_SIZE[0], configs_param.INPUT_SIZE[1]),
        batch_size=configs_param.BATCH_SIZE,
        # label_mode='categorical'
    )
    test_ds = tf.keras.preprocessing.image_dataset_from_directory(
        test_dir,
        seed=123,
        image_size=(configs_param.INPUT_SIZE[0], configs_param.INPUT_SIZE[1]),
        batch_size=configs_param.BATCH_SIZE,
        # label_mode='categorical'
    )
    """
    <BatchDataset shapes: ((None, 180, 180, 3), (None,)), types: (tf.float32, tf.int32)>
    <class 'tensorflow.python.data.ops.dataset_ops.BatchDataset'>
    """
    class_names = train_ds.class_names
    print(class_names)
    for image_batch, labels_batch in train_ds:
        print(image_batch.shape)
        print(labels_batch.shape)
        break
    # normalized_ds = train_ds.map(lambda x, y: (normalization_layer(x), y))
    # image_batch, labels_batch = next(iter(normalized_ds))

    AUTOTUNE = tf.data.experimental.AUTOTUNE
    train_ds = train_ds.cache().shuffle(1000).prefetch(buffer_size=AUTOTUNE)
    val_ds = valid_ds.cache().prefetch(buffer_size=AUTOTUNE)
    test_ds = test_ds.cache().prefetch(buffer_size=AUTOTUNE)

    """
    # print(type(train_ds))
    # print(train_ds)
    <class 'tensorflow.python.data.ops.dataset_ops.PrefetchDataset'>
    <PrefetchDataset shapes: ((None, 180, 180, 3), (None,)), types: (tf.float32, tf.int32)>
    """
    return train_ds, valid_ds, test_ds


if __name__ == "__main__":
    create_datasets()
