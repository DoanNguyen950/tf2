##              Basic libaries
import sys
import os
from pathlib import Path
import argparse
current_path = Path(os.getcwd())




###             Paths 
CKPT_DIR = os.path.join(str(current_path), 'checkpoints')
LOCAL_WEIGHTS_FILE = ''
MODEL_RESULTS = 'first_test.h5'
# TRAIN_PATH = '/root/datasets/SealDataset_TrainValdTest_ver2/train'
# VALID_PATH = '/root/datasets/SealDataset_TrainValdTest_ver2/valid'
# TEST_PATH = '/root/datasets/SealDataset_TrainValdTest_ver2/test'

TRAIN_PATH = '/content/drive/My Drive/Backup/Image_classification/datasets/SealDataset_TrainValdTest_ver2/train'
VALID_PATH = '/content/drive/My Drive/Backup/Image_classification/datasets/SealDataset_TrainValdTest_ver2/valid'
TEST_PATH = '/content/drive/My Drive/Backup/Image_classification/datasets/SealDataset_TrainValdTest_ver2/test'
LOG_DIR = './logs/scalars'
CKPT_PATH = '/home/Tensorflow_2.0/TF_2.0_Tutorials/Eat_TF2/Chapter1.ModelingProcedureOfTensorflow/Chapter1-2.ModelingProcedureForImages/checkpoints'


###             Parameters
IMAGE_SIZE = (227, 227, 3)
INPUT_SIZE = (227, 227, 3)

image_height = 227
image_width = 227 
channels = 3
EPOCHS = 10
BATCH_SIZE = 64
NUMB_WORKER = 4
classes = len(os.listdir(TRAIN_PATH))
NUM_CLASSES = classes





def parse_args():
    """         Configs the parameters for model            """
    parser = argparse.ArgumentParser(description='Object Classification')

    ###             Paths config
    parser.add_argument('--checkpoint_path', dest='checkpoint_path',
                        default="checkpoints/model.ckpt-400000",
                        help='the path of pretrained model to be used', type=str)

    parser.add_argument('--train_dir', dest='train_dir', 
                        default='/media/doannn/data/Projects/Works/SealProject/Datasets/data_from_internet/cifar10_imgs/train',
                        help='The directory where the train files are stored.', type=str)

    parser.add_argument('--test_dir', dest='test_dir', 
                        default='/media/doannn/data/Projects/Works/SealProject/Datasets/data_from_internet/cifar10_imgs/test',
                        help='The directory where the test files are stored.', type=str)

    """                     Config arguments                                """             
    ##          Datasets
    parser.add_argument('--eval_image_width', dest='eval_image_width',
                        help='resized image width for inference',
                        default=1280, type=int)
    parser.add_argument('--eval_image_height', dest='eval_image_height',
                        help='resized image height for inference',
                        default=768, type=int)   
    parser.add_argument('--pixel_conf_threshold', dest='pixel_conf_threshold',
                        help='threshold on the pixel confidence',
                        default=0.5, type=float) 
    parser.add_argument('--link_conf_threshold', dest='link_conf_threshold',
                        help='threshold on the link confidence',
                        default=0.5, type=float) 
    parser.add_argument('--moving_average_decay', dest='moving_average_decay',
                        help='The decay rate of ExponentionalMovingAverage',
                        default=0.9999, type=float)               
    ###         Models
    parser.add_argument('--gpu_memory_fraction', dest='gpu_memory_fraction',
                        help='the gpu memory fraction to be used. If less than 0, allow_growth = True is used.',
                        default=0, type=float)
    parser.add_argument('--batch_size', dest='batch_size', default=16,
                            help='the batch size', type=float)
                              
                        
    global args
    args = parser.parse_args()
    return args

