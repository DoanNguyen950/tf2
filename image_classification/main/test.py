##              Basic libaries
import sys
import os
from pathlib import Path
import numpy as np
##              Framework
import tensorflow as tf
from tensorflow.keras.models import load_model
##              Files
current_path = Path(os.getcwd())
sys.path.append(str(current_path))
import configs_param
from models.simple_convnet import simple_model
##              Logging

def load_trained_model(weights_path):
    model = simple_model()
    model.load_weights(weights_path)
    return model


def test_single_image(img_path):
    """         Test predict a image
    Args:
        img_path (str): the image's path
    Return:
        class_score (float): the predict's probability
    """
    img = tf.keras.preprocessing.image.load_img(
        '/home/doannn/Documents/Public/bitbucket/datasets/cifar100_imgs/test_image/0005.png'
    )
    img_array = tf.keras.preprocessing.image.img_to_array(img)
    img_array = tf.expand_dims(img_array, 0)

    pred = model.predict(img_array)
    print(pred)
    score = pred[0]
    return score

if __name__ == '__main__':
    ckpt_path = os.path.join(configs_param.CKPT_DIR, configs_param.MODEL_RESULTS)
    model = load_model(ckpt_path)
    cls = test_single_image('/home/doannn/Documents/Public/bitbucket/datasets/cifar100_imgs/test_image/0005.png')
    print(cls)