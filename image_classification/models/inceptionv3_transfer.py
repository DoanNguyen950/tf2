##          basic libaries
import os
import sys
from pathlib import Path
##          Framework
from tensorflow.keras.applications import InceptionV3
##          Files
import sys
import os
from pathlib import Path, PosixPath
current_path = Path(os.getcwd())
sys.path.append(str(current_path))



def inception_transfer():
    inception_model = InceptionV3(weights='imagenet', include_top=False)
    ###     add customer classification layer
    x = inception_model.output
    x = layers.GlobalAveragePooling2D(name='avg_pool')(x)
    predictions = layers.Dense(configs_param.NUM_CLASSES, activation='softmax')(x)
    model = Model(inputs=inception_model.input, outputs=predictions)

    ###         Transfer learning
    for layer in inception_model.layers:
        layer.trainable=False 
    model.summary()

    return model
