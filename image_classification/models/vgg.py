"""         VGG-16

"""
##          Basic libaries
import os
import sys
from pathlib import Path
##          Framework
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers, models
##          Files
from pathlib import Path
root_path=Path(os.getcwd())
sys.path.append(str(root_path))
import configs_param


def vgg():
    tf.keras.backend.clear_session()
    in_shape=configs_param.INPUT_SIZE
    n_classes=configs_param.NUM_CLASSES

    ###                             Extract features blocks
    inputs = layers.Input(shape=in_shape)
    print()
    ##          Block 1
    conv1_1 = layers.Conv2D(filters=64, kernel_size=(3, 3),
                            activation='relu', padding='same',
                            name='block1_conv1')(inputs)
    conv1_2 = layers.Conv2D(filters=64, kernel_size=(3, 3),
                            activation='relu', padding='same',
                            name='block1_conv2')(conv1_1)
    maxpooling1 = layers.MaxPooling2D((2, 2), strides=(2, 2), name='block1_pool')(conv1_2)

    ##          Block 2
    conv2_1 = layers.Conv2D(filters=128, kernel_size=(3, 3),
                            activation='relu', padding='same',
                            name='block2_conv1')(maxpooling1)
    conv2_2 = layers.Conv2D(filters=128, kernel_size=(3, 3),
                            activation='relu', padding='same',
                            name='block2_conv2')(conv2_1)
    maxpooling2 = layers.MaxPooling2D((2, 2), strides=(2, 2), name='block2_pool')(conv2_2)

    ##          Block 3
    conv3_1 = layers.Conv2D(filters=256, kernel_size=(3, 3),
                            activation='relu', padding='same',
                            name='block3_conv1')(maxpooling2)
    conv3_2 = layers.Conv2D(filters=256, kernel_size=(3, 3),
                            activation='relu', padding='same',
                            name='block3_conv2')(conv3_1)
    maxpooling3 = layers.MaxPooling2D((2, 2), strides=(2, 2), name='block3_pool')(conv3_2)

    ##          Block 4
    conv4_1 = layers.Conv2D(filters=512, kernel_size=(3, 3),
                            activation='relu', padding='same',
                            name='block4_conv1')(maxpooling3)
    conv4_2 = layers.Conv2D(filters=512, kernel_size=(3, 3),
                            activation='relu', padding='same',
                            name='block4_conv2')(conv4_1)
    conv4_2 = layers.Conv2D(filters=512, kernel_size=(3, 3),
                            activation='relu', padding='same',
                            name='block4_conv3')(conv4_2)
    maxpooling4 = layers.MaxPooling2D((2, 2), strides=(2, 2), name='block4_pool')(conv4_2)

    ##          Block 5
    conv5_1 = layers.Conv2D(filters=512, kernel_size=(3, 3),
                            activation='relu', padding='same',
                            name='block5_conv1')(maxpooling4)
    conv5_2 = layers.Conv2D(filters=512, kernel_size=(3, 3),
                            activation='relu', padding='same',
                            name='block5_conv2')(conv5_1)
    conv5_2 = layers.Conv2D(filters=512, kernel_size=(3, 3),
                            activation='relu', padding='same',
                            name='block5_conv3')(conv5_2)
    maxpooling4 = layers.MaxPooling2D((2, 2), strides=(2, 2), name='block5_pool')(conv5_2)

    ###                             Classification blocks
    flatten = layers.Flatten(name='flatten')(maxpooling4)
    dense1 = layers.Dense(4096, activation='relu', name='fc1')(flatten)
    
    dense2 = layers.Dense(4096, activation='relu', name='fc2')(dense1)
    outputs = layers.Dense(n_classes, activation='softmax', name='predictions')(dense2)

    model = models.Model(inputs=inputs, outputs=outputs, name='vgg16')
    model.summary()
    return model

