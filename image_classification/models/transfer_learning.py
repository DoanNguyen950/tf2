"""     This function transfers learning for image classification
The model transfer:
    - Resnet
    - InceptionV3
    - VGG
"""
##          Basic libaries
import os
import sys
from pathlib import Path
##          Framework
import tensorflow as tf
from tensorflow.keras.preprocessing.image import load_img, img_to_array
from tensorflow.keras import layers, Model
from tensorflow.keras.optimizers import RMSprop
from tensorflow.keras.applications.inception_v3 import InceptionV3
from tensorflow.keras.applications.vgg16 import VGG16
##          Files
sys.path.append('../')
import configs_param


###             Metrics for imbalance datasets
METRICS = [
      tf.keras.metrics.TruePositives(name='tp'),
      tf.keras.metrics.FalsePositives(name='fp'),
      tf.keras.metrics.TrueNegatives(name='tn'),
      tf.keras.metrics.FalseNegatives(name='fn'), 
      tf.keras.metrics.BinaryAccuracy(name='accuracy'),
      tf.keras.metrics.Precision(name='precision'),
      tf.keras.metrics.Recall(name='recall'),
      tf.keras.metrics.AUC(name='auc'),
]


def inceptionv3_pretrained():
    pre_trained_model = InceptionV3(
        input_shape=configs_param.INPUT_SHAPE,
        include_top=False,
        weigths='imagenet'
    )
    pre_trained_model.load_weights(configs_param.LOCAL_WEIGHTS_FILE)
    for layer in pre_trained_model.layers:
        layer.trainable=False
    last_layer = pre_trained_model.get_layer('mixed7')
    print("The last layer output shape: {}".format(last_layer.output_shape))
    last_output = last_layer.output

    x = layers.Flatten()(last_output)
    x = layers.Dense(1024, activation='relu')(x)
    x = layers.Dropout(0.1)(x)
    x = layers.Dense(configs_param.NUM_CLASSES, activation='softmax')(x)

    model = Model(pre_trained_model.input, x)

    return model


def vgg16_pretrained():
    pretrained_model = VGG16(
        input_shape=configs_param.IMAGE_SIZE,
        include_top=False,
        weights='imagenet')
    pretrained_model.summary()
    for layer in pretrained_model.layers:
        layer.trainable=False
    
    ###         Pretrained summary()
    last_layer=pretrained_model.get_layer('block5_pool')
    print("The last layer of VGG: {}".format(last_layer.output_shape))
    last_output=last_layer.output

    x = layers.Flatten()(last_output)
    x = layers.Dense(1024, activation='relu')(x)
    x = layers.Dropout(0.1)(x)
    x = layers.Dense(configs_param.NUM_CLASSES, activation='softmax')(x)

    model_vgg=Model(pretrained_model.input, x)

    return model_vgg


# def customer_train(metrics=METRICS, output_bias=None):
#     if output_bias is not None:
#         output_bias = tf.keras.initializers.Constant(output_bias)
#     model_vgg = vgg16_pretrained()



