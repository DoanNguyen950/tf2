##          basic libaries
import os
import sys
from pathlib import Path
##          Framework
from tensorflow.keras.applications.vgg16 import VGG16
from tensorflow.keras.models import Model
# from tensorflow.keras.utils import np_utils
from tensorflow.keras.models import Sequential
from tensorflow.keras.callbacks import EarlyStopping, History, ModelCheckpoint
from tensorflow.keras.layers import Flatten, Dense, Dropout, Reshape, Lambda, BatchNormalization
##          Files
import sys
import os
from pathlib import Path, PosixPath
current_path = Path(os.getcwd())
sys.path.append(str(current_path))
import configs_param


def vgg16_transfer():
    vgg16_base = VGG16(weights='imagenet', include_top=False, 
                        input_tensor=None, input_shape=configs_param.INPUT_SIZE)
    ###     add customer classification layer
    output = vgg16_base.get_layer(index=-1).output
    output = Flatten()(output)
    ##      Add fully-connected layer
    output = Dense(4096, activation='relu')(output)
    output = BatchNormalization()(output)
    output = Dense(512, activation='relu')(output)
    output = BatchNormalization()(output)

    output = Dense(configs_param.NUM_CLASSES, activation='softmax')(output)

    vgg16_model = Model(vgg16_base.input, output)

    ###         Transfer learning
    for layer in vgg16_model.layers[:19]:
        layer.trainable=False 
    vgg16_model.summary()

    return vgg16_model
