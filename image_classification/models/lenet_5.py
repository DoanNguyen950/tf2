import tensorflow as tf 
import tensorflow.keras as keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Flatten, Dense


def lenet():
    model = Sequential()
    ##      Convolutional 1
    model.add(Conv2D(
        filters=6,
        kernel_size=5,
        strides=1,
        activation='relu',
        input_shape=(32, 32, 1))
    )
    model.add(MaxPooling2D(pool_size=2, strides=2))
    ##      Convolutional 2
    model.add(Conv2D(
        filters=16,
        kernel_size=5,
        strides=1,
        activation='relu',
        input_shape=(14, 14, 6))
    )
    model.add(MaxPooling2D(pool_size=2, strides=2))

    ##      Flatten
    model.add(Dense(units=120, activation='relu'))
    model.add(Dense(units=84, activation='relu'))
    ##      Output layer
    model.add(Dense(units=10, activation='softmax'))
    model.summary()

    return model
