##              Framework
import tensorflow as tf
from tensorflow.keras import layers, datasets, models
##              Files
import sys
import os
from pathlib import Path
current_path = Path(os.getcwd())
sys.path.append(str(current_path))
import configs_param


def simple_model():
    tf.keras.backend.clear_session()

    inputs = layers.Input(shape=(32, 32, 3))
    x = layers.Conv2D(32,kernel_size=(3,3))(inputs)
    x = layers.MaxPool2D()(x)
    x = layers.Conv2D(64,kernel_size=(5,5))(x)
    x = layers.MaxPool2D()(x)
    x = layers.Dropout(rate=0.1)(x)
    x = layers.Flatten()(x)
    x = layers.Dense(32,activation='relu')(x)
    outputs = layers.Dense(configs_param.NUM_CLASSES, activation = 'sigmoid')(x)
    # print("Outputs: {}".format(outputs))
    print("11111")
    model = models.Model(inputs=inputs, outputs=outputs)
    model.summary()

    return model 



def simple_model():
    tf.keras.backend.clear_session()

    num_classes = 5

    model = Sequential([
    layers.experimental.preprocessing.Rescaling(1./255, input_shape=(img_height, img_width, 3)),
    layers.Conv2D(16, 3, padding='same', activation='relu'),
    layers.MaxPooling2D(),
    layers.Conv2D(32, 3, padding='same', activation='relu'),
    layers.MaxPooling2D(),
    layers.Conv2D(64, 3, padding='same', activation='relu'),
    layers.MaxPooling2D(),
    layers.Flatten(),
    layers.Dense(128, activation='relu'),
    layers.Dense(num_classes)
    ])

    
    model.summary()

    return model 

if __name__ == '__main__':
    simple_model()
