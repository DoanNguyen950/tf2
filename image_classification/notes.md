## 1.2 Xây dựng mô hình cho dữ liệu ảnh

+ Xem xét tập dữ liệu ảnh *cifar2* một tập nhỏ trong bộ dữ liệu *cifar-10*. Dữ liệu được sắp xếp:
```
cifar2_dataset:
    - train
        - 0_aiplain
            - img_1.jpg
            - img_2.jpg
            ...
        - 1_automobile
            - img_a.jpg
            - img_b.jpg
    - test
        - 0_aiplain
            - img_10.jpg
            - img_20.jpg
            ...
        - 1_automobile
            - img_a1.jpg
            - img_b1.jpg 
```

+ Có 2 cách chuẩn bị dữ liệu trong TensorFlow:
    - Cách 1: Xây dựng mã hóa dạng mô hình dữ liệu ảnh sử dụng *ImageDataGenerator* trong keras.
    - Cách 2: Xây dựng dòng dữ liệu sử dụng *tf.data.Dataset* & một vài phương thức *tf.images*
=> Phương thức tổ chức dữ liệu dạng dòng sẽ linh hoạt với khả năng nâng cao hiệu suất.

+ Phương thức xây dựng model:
    - Có 3 phương thức xây dụng model sử dụng APIs của Keras:
        - Sử dụng chuỗi modeling *Sequential()*
        - Sử dụng functional API
        - Tùy chỉnh kiến trúc mô hình bởi class *Model*

+ Phương thức train cho model: 
    - Sử dụng *fit()*
    - Sử dụng *train_on_batch()*
    - Tùy chỉnh vòng lặp training.
=> Cách đơn giản nhất sử dụng fit()


+ Phương thức save model:
    - Lưu trọng số, đây là cách duy nhất lưu các tensors của trọng số:
    ```
    model.save_weights(path_ckpt/weight.ckpt, save_format='tf')
    ```
    
    - Lưu cấu trúc của model & các tham số vào 1 file, với cách này t có thể sử dụng đê deployment: 
    ```
    model.save('../model_savedmodel', save_format='tf')
    ```

+ Loaded model đã lưu:
    ```
    model_loaded = tf.keras.models.load_model('../model_savedmodel')
    model_loaded.evaluate(ds_test)
    ```

+ Tính toán parameter trong 1 lớp conv:
    - paramter = ((w*h*previous_filter)+1)*current_filter
    ```
    - input_shape=3:
        -> (3+1)*4
        -> (4+1)*5
        -> (5+1)*2
    ```

## 1.2.1 Cơ chế tích chập (convolutional) và tương quan chéo (cross correlation)

+ Thuật toán áp dụng trong CNN là cross correlation (tương quan chéo) chứ không phải convolution.
    - Phân tích về sư khác nhau giữa Correlation & Convolution mình có viết 1 bài ở [đây](https://github.com/Doan-Nguyen/Deep_Learning_Notes/blob/master/Quickly_notes/PhanBietConvolutionalCorrelation/phan_biet_convolution_crosscorrelation.md) 
+ Một vài nhận xét:
    - Correlation là một hàm thực hiện bằng việc *displacement* của bộ lọc filter trên mặt ảnh gốc.
    

## 1.2.2 Hyperparameter search
+ 